libdist-zilla-localetextdomain-perl (0.91-4) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:38:25 +0100

libdist-zilla-localetextdomain-perl (0.91-3) unstable; urgency=medium

  * Team upload.
  * Add a (build) dependency on liblocale-codes-perl.
    Thanks to Niko Tyni for the bug report. (Closes: #903271)
  * Declare compliance with Debian Policy 4.1.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Jul 2018 16:39:33 +0200

libdist-zilla-localetextdomain-perl (0.91-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Add patch from upstream Git repo to replace Email::Address with
    Email::Address::XS.
    Thanks to Pali Rohár for the bug report and the patch.
    (Closes: #887539)
  * debian/control: libemail-address-perl → libemail-address-xs-perl.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Mon, 02 Jul 2018 21:04:05 +0200

libdist-zilla-localetextdomain-perl (0.91-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Florian Schlichting ]
  * Import upstream version 0.91 (closes: #825311)
  * Switch (build-)dependencies from lib(moosex-types-)path-class-perl to
    lib(moosex-types-)path-tiny-perl
  * Bump copyright years
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 4.1.1

 -- Florian Schlichting <fsfs@debian.org>  Sat, 11 Nov 2017 17:05:56 +0100

libdist-zilla-localetextdomain-perl (0.90-2) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata.
  * Add explicit build dependency on libmodule-build-perl.

 -- gregor herrmann <gregoa@debian.org>  Tue, 09 Jun 2015 23:58:40 +0200

libdist-zilla-localetextdomain-perl (0.90-1) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Axel Beckert ]
  * Import new upstream release
  * Bump Standards-Version to 3.9.6 (no changes needed)
  * Fix pkg-perl-specific lintian warning missing-testsuite-header

 -- Axel Beckert <abe@debian.org>  Sat, 11 Oct 2014 02:32:27 +0200

libdist-zilla-localetextdomain-perl (0.87-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2013 21:38:02 +0100

libdist-zilla-localetextdomain-perl (0.86-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Sep 2013 19:26:31 +0200

libdist-zilla-localetextdomain-perl (0.85-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update years of upstream copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Feb 2013 17:37:34 +0100

libdist-zilla-localetextdomain-perl (0.83-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Drop patches, both applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Dec 2012 16:27:02 +0100

libdist-zilla-localetextdomain-perl (0.81-1) unstable; urgency=low

  * Initial Release. (Closes: #690850)

 -- Joenio Costa <joenio@colivre.coop.br>  Thu, 18 Oct 2012 11:16:36 -0300
